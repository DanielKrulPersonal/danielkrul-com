export interface Article {
    title: string,
    text: string,
    date: string,
    tag: string,
    url: string
}
