import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import VueAnalytics from 'vue-analytics';
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/js/all.js';
import moment from "moment";
const VueMarkdown = require('vue-markdown');

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app");

Vue.component('vue-markdown', VueMarkdown.default);

Vue.use(VueAnalytics, {
  id: 'UA-42571558-7',
  router
});

Vue.filter('formatDate', (value: string) => {
  if (value) {
    return moment(String(value)).format('DD. MM. YYYY hh:mm');
  }
});
