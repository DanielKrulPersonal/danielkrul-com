import { Vue, Component } from 'vue-property-decorator'
import axios from "axios";
import {Article} from "@/interfaces/article";

@Component
export class ApiMixin extends Vue {
    public async getArticles(): Promise<Article[]> {
        const resp = await axios.get(`${process.env.VUE_APP_FUNCTIONS_BASE}/articles-list`);
        return resp.data;
    }
    public async getArticle(url: string): Promise<Article> {
        const resp = await axios.get(`${process.env.VUE_APP_FUNCTIONS_BASE}/articles-detail?url=${url}`);
        return resp.data;
    }
}
