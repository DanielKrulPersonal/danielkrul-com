// [START import]
import * as functions from 'firebase-functions';
import * as mongoose from "mongoose";
import Article from "../models/article";
import {DB_URL, DEV} from "../utils";
// [END import]

export const list = functions.region('europe-west1').https.onRequest(async (request, response) => {
    if(mongoose.STATES[mongoose.connection.readyState] !== 'connected') {
        await mongoose.connect(DB_URL, { useFindAndModify: false, useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });
    }

    if(DEV) {
        response.set('Access-Control-Allow-Origin', '*');
    }

    const result = await Article.find();
    response.status(200).send(result);
});
