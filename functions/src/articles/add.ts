// [START import]
import * as functions from 'firebase-functions';
import * as mongoose from "mongoose";
import Article from "../models/article";
import {DB_URL} from "../utils";
const getSlug = require('speakingurl');
// [END import]

export const add = functions.region('europe-west1').https.onRequest(async (request, response) => {
    const title = request.body.title;
    const text = request.body.text;
    const date = request.body.date;
    const tag = request.body.tag;
    if(request.header('Authorization') !== 'Bearer ') {
        throw new functions.https.HttpsError(
            'failed-precondition',
            'The function must be called while authenticated.'
        );
    }

    if (
        !(typeof title === 'string') || title.length <= 2 ||
        !(typeof text === 'string') || text.length <= 2 ||
        !(typeof date === 'string') ||
        !(typeof tag === 'string')
    ) {
        // Throwing an HttpsError so that the client gets the error details.
        throw new functions.https.HttpsError('invalid-argument', 'Wrong data');
    }

    if(mongoose.STATES[mongoose.connection.readyState] !== 'connected') {
        await mongoose.connect(DB_URL, { useFindAndModify: false, useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });
    }

    const url = getSlug(title);
    const result = await Article.create({
        title, text, date, tag, url
    });
    response.status(200).send(result);
});
