import * as mongoose from "mongoose";

const articleSchema = new mongoose.Schema({
    title: {
        type: String
    },
    text: {
        type: String
    },
    date: {
        type: Date
    },
    tag: {
        type: String
    },
    url: {
        type: String
    }
});
//articleSchema.index({ name: 1 }, { unique: true });
const Article = mongoose.model('Article', articleSchema);
//Article.createIndexes();

export default Article;
