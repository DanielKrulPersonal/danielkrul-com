import * as admin from 'firebase-admin';

// The Firebase Admin SDK to access the Firebase Firestore DB.
admin.initializeApp();

exports.articles = {
    list: require('./articles/list').list,
    add: require('./articles/add').add,
    detail: require('./articles/detail').detail
}
